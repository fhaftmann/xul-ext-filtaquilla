
Pragmatic debian packaging for FiltaQuilla for Thunderbird
==========================================================


See also
--------

https://addons.thunderbird.net/de/thunderbird/addon/filtaquilla/


Building
--------

    $ dpkg-buildpackage


Obtaining a new upstream version
--------------------------------

    $ git remote add upstream https://github.com/RealRaven2000/FiltaQuilla
    $ git fetch upstream
    $ git checkout debian
    $ git merge … # version tag
